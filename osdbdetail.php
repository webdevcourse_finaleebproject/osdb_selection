<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Databases info</title>
<!-- MDB icon -->
<!-- <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon"> -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<link href='https://fonts.googleapis.com/css?family=Aref Ruqaa' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Artifika' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Autour One' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Carter One' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Poller One' rel='stylesheet'>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather:400,900,900i">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-responsive.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="../css/mdb.min.css">
<!-- Your custom styles (optional) -->
<link rel='stylesheet' type='text/css' href='../css/osdb.css'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="../js/csi.js"></script>


<script>

function myFunction() {
  var input = document.getElementById("search");
  var filter = input.value.toLowerCase();
  var nodes1 = document.getElementsByClassName('col-sm-11');
  var nodes2 = document.getElementsByClassName('searchf');
  var sub = document.getElementsByClassName('category');
  var localFooter = document.getElementsByTagName('footer');

  if (filter.length > 0)
  {
	  sub[0].style.display = "none";
	  sub[1].style.display = "none";
  }
  else{
		sub[0].style.display = "block";
		sub[1].style.display = "block";
  }


  for (i = 0; i <= nodes1.length; i++) {
    if (nodes1[i].innerText.toLowerCase().includes(filter)) {
      nodes1[i].style.display = "block";
	  nodes2[i].style.display = "block";
    } else {
      nodes1[i].style.display = "none";
	  nodes2[i].style.display = "none";
    }
  }
}

</script>

</head>

<body class="bg-orange">
<main>
<header data-include="headerF.html"></header>

<div class="container">
<div class="row">
<div class="col-12">

<form class="float-right img-fluid">
       <input class="form-control mr-sm-2" onkeyup="myFunction()" id="search" type="text" placeholder="Search" aria-label="Search">
</form>
</div>
</div>
</div>

<section>
<div class="container" style="min-height:600px">
  <div class="category">
    <h1> Operating Systems </h1>
  </div>
	<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>CentOS</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/centos.jpg" alt="icon" width="80px"></div>
        <div class="col-sm-11">
        <p class="searchDB">
            CentOS is a Linux distribution that provides a free, community-supported computing platform functionally compatible with its upstream source
            Red Hat Enterprise Linux (RHEL)
         </p>
         </div>
    </div>
</div>
    <div class="searchf">
	<div class="row borderow">
        <div class="col-sm-10">
            <h2>HP-UX</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/hpux.jpg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
             HP-UX (from "Hewlett Packard Unix") is Hewlett Packard Enterprise's proprietary implementation of the Unix operating system
			 based on Unix System V (initially System III) and first released in 1984.
         </p>
         </div>
    </div>
	</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>IBM AIX</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/aix.jpg" alt="icon" width="70px"></div>
        <div class="col-sm-11">
        <p>
            is a series of proprietary Unix operating systems developed and sold by IBM for several of its computer platforms.
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Microsoft Windows</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="2019.jpg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            is a group of several proprietary graphical operating system families, all of which are developed and marketed by Microsoft
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Oracle Linux</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/oracle.png" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            Oracle Linux is a Linux distribution packaged and freely distributed by Oracle,
			available partially under the GNU General Public License since late 2006.
			It is compiled from Red Hat Enterprise Linux (RHEL) source code, replacing Red Hat branding with Oracle's.
			It is also used by Oracle Cloud and Oracle Engineered Systems such as Oracle Exadata and others.
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Red Hat Enterprise Linux</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/red-hat.png" alt="icon" width="150px" height="150px"></div>
        <div class="col-sm-11">
        <p>
            Red Hat Enterprise Linux is a Linux distribution developed by Red Hat for the commercial market.
			Red Hat Enterprise Linux is released in server versions for x86-64, Power ISA, ARM64, and IBM Z and a desktop version for x86-64.
         </p>
         </div>
    </div>
	</div>
	<div class="searchf">
	<div class="row borderow">
        <div class="col-sm-10">
            <h2>Solaris</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/solaris.jpg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            Solaris is a proprietary Unix operating system originally developed by Sun Microsystems.
			It superseded the company's earlier SunOS in 1993. In 2010, after the Sun acquisition by Oracle, it was renamed Oracle Solaris.
         </p>
         </div>
    </div>
</div>
<div class="searchf">
	<div class="row borderow">
        <div class="col-sm-10">
            <h2>Ubuntu</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/OS/ubuntu.jpg" alt="icon" width="200px" height="200px"></div>
        <div class="col-sm-11">
        <p>
           Ubuntu is a Linux distribution based on Debian mostly composed of free and open-source software.
		   Ubuntu is officially released in three editions: Desktop, Server, and Core for Internet of things devices and robots.
		   All the editions can run on the computer alone, or in a virtual machine.
		   Ubuntu is a popular operating system for cloud computing, with support for OpenStack.
         </p>
         </div>
    </div>
	</div>
    <h1 class="category"> Databases </h1>
	<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Cloudera Hadoop</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/claudera.jpg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            Cloudera, Inc. is a US-based software company that provides a software platform for data engineering,
			data warehousing, machine learning and analytics that runs in the cloud or on premises.
         </p>
         </div>
    </div>
	</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>IBM Db2</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/ibm-db2.png" alt="icon" width="70px"></div>
        <div class="col-sm-11">
        <p>
            Db2 is a family of data management products, including database servers, developed by IBM.
			They initially supported the relational model, but were extended to support object-relational features and non-relational structures like JSON and XML.
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Informix</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/informix.jpeg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            IBM Informix is a product family within IBM's Information Management division that is centered on several relational database management system (RDBMS) offerings.
			The Informix products were originally developed by Informix Corporation, whose Informix Software subsidiary was acquired by IBM in 2001.
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>MariaDB Server</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/mariadb.jpg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            MariaDB is a community-developed, commercially supported fork of the MySQL relational database management system (RDBMS),
			intended to remain free and open-source software under the GNU General Public License
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Microsoft SQL Server</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/sqlserver.jpg" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            Microsoft SQL Server is a relational database management system developed by Microsoft.
			As a database server, it is a software product with the primary function of storing and retrieving data
			as requested by other software applications—which may run either on the same computer or on another computer across a network (including the Internet)
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>MongoDB</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/mongodb.png" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            MongoDB is a cross-platform document-oriented database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with optional schemas.
			MongoDB is developed by MongoDB Inc. and licensed under the Server Side Public License (SSPL).
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>MySQL</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/mySQL.png" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            MySQL is an open-source relational database management system (RDBMS). Its name is a combination of "My", the name of co-founder Michael Widenius's daughter and "SQL", the abbreviation for Structured Query Language.
         </p>
         </div>
    </div>
</div>
<div class="searchf">
    <div class="row borderow">
        <div class="col-sm-10">
            <h2>Oracle Databases</h2>
        </div>
        <div class="col-sm-2"><img class="float-right img-fluid" src="../resources/DB/oracledb.png" alt="icon" width="100px"></div>
        <div class="col-sm-11">
        <p>
            Oracle Databases is a database commonly used for running online transaction processing (OLTP), data warehousing (DW) and mixed (OLTP & DW) database workloads.
        </p>
        </div>
     </div>
</div>

</section>

</div>


<footer data-include="footerF.html"></footer>

</main>

</body>
</html>

<?php
include "osdb_pages/mysqlConfigCpanel.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>WEBcourse | MTA</title>
    <script src="js/csi.js"></script>
<!--
    <link href='https://fonts.googleapis.com/css?family=Carter One' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Poller One' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-responsive.css">
    <link rel="stylesheet" href="css/mdb.min.css">
    <link rel="stylesheet" href="css/style.css"> -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Carter One' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Poller One' rel='stylesheet'>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-responsive.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" type='text/css' href="css/style.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<header data-include="header.html"></header>

  <a id="topButton"></a>

  <script>
    var btn = $('#topButton');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 0) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on("click", function (e) {
      e.preventDefault();
      $("html").animate({ scrollTop: 0 }, "500");
    });

  </script>

<div style="margin:1em">
<?php
if (isset($_COOKIE['first_name'])) {
    $msg = $_COOKIE['first_name'];
    echo "<span style='color:blue' clickable='false'>Welcome $msg</span>";
} else {
    echo "<label>Hi, <a style='color:blue' onclick=\"document.getElementById('modalView').style.display = 'block';\">Login</a></label>";
}
?>
</div>

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 style="font-family:'Poller One';font-size:50px">DBOSS - DB/OS creation tool</h1>
        <H3>The utility which makes DB server creation more easy.</H3>
    </div>
</div>

<section id="howToUse">
    <div class="row">
        <div class="column2">
            <div class="jumbotron jumbotron2 jumbotron-fluid">
                <div class="container">
                    <h1 style="font-size:50px">How to use DBOSS tool?</h1>
                    <H4>1. Pick the desired permutation</H4>
                    <H4>2. Choose resources</H4>
                    <H4>3. Let it run!</H4>
                </div>
                <div class="center-con">
                    <a class="navbar-brand" href="index.php#create">
                        <div class="round">
                            <span class="span1"></span>
                            <span class="span1"></span>
                            <span class="span1"></span>
                            <span class="span1"></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="column2">
            <div class="embed-responsive" style="height:100%">
                 <iframe class="embeddedObject shadow resizable" name="embedded_content" scrolling="no" frameborder="0" type="text/html"
        style="overflow:hidden;" src="https://www.screencast.com/users/matanhury/folders/Default/media/9d2e073c-938b-400f-b4dd-81476937e023/embed" height="932" width="1888" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
<section id="create">
    <div class="row">
        <!-- OS -->
        <div class="column" id="osClumm" style="background-color:#aaa;">
            <div id="osContainer">
                <div class="OsList">
                    <h2>Pick an Operation System</h2>
                    <table class="table table-hover">
                        <thead class="thead-light" style="font-size:5em;">
                        <tr>
                            <th><strong>NAME</strong></th>
                            <th><strong>VERSION</strong></th>
                        </tr>
                        </thead>
                        <tr>
                            <td>RED HAT</td>
                            <td id="RHEL">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $osVer_RHEL = "select distinct osMajorVer from permutations where osType like 'RHEL';";
                                    $osVer_RHEL_data = mysqli_query($link, $osVer_RHEL);
                                    while ($row = mysqli_fetch_assoc($osVer_RHEL_data)) {
                                        $osVer = $row['osType'];
                                        $RHELMajorVer = $row['osMajorVer'];
                                        echo "<option>" . $RHELMajorVer . "</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>OEL UEK</td>
                            <td id="OELUEK">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct osMajorVer from permutations where osType like 'OELUEK';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["osMajorVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>CentOS</td>
                            <td id="centos">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct osMajorVer from permutations where osType like 'CENTOS';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["osMajorVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>SUSE</td>
                            <td id="SUSE">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct concat (osMajorVer,osMinorVer) from permutations where osType like 'SUSE';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["concat (osMajorVer,osMinorVer)"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>UBUNTU</td>
                            <td id="UBUNTU">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct concat (osMajorVer,osMinorVer) from permutations where osType like 'UBUNTU';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["concat (osMajorVer,osMinorVer)"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>OEL</td>
                            <td id="OEL">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct osMajorVer from permutations where osType like 'OEL';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["osMajorVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Windows</td>
                            <td id="WINDOWS">
                                <select onchange="osShow(this.parentNode.id,this)">
                                    <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct osMajorVer from permutations where osType like 'WIN';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["osMajorVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>


                    </table>
                </div>
            </div>
            <div>
                <lable id="osLbl1">Your choose: <H1 id="osLbl2"></H1></lable>
                <a id="backOsContainer" onclick="backToOsContainer()" style="color:white" ;>Back</a>
            </div>
        </div>

        <!-- DB -->

        <div class="column" id="dbClumm" style="background-color:#bbb;">
            <div id="dbContainer">
                <div class="dbList">
                    <h2>Pick a Database</h2>
                    <table class="table table-hover">

                        <thead class="thead-light" style="font-size:5em;">
                        <tr>
                            <th><strong>NAME</strong></th>
                            <th><strong>VERSION</strong></th>
                        </tr>
                        </thead>

                        <tr>
                            <td>ORACLE</td>
                            <td>
                                <div id="ORACLE">
                                    <select onchange="dbShow(this.parentNode.id,this)">
                                      <option value="NoData">- Select -</option>
                                        <?php
                                        $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'Oracle';");
                                        while ($row = mysqli_fetch_array($res)) {
                                            ?>
                                            <option><?php echo $row["dbVer"]; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>DB2</td>
                            <td>
                            <div id="DB2">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'DB2';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>MsSQL</td>
                            <td>
                            <div id="MsSQL">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'MsSQL';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>HANA</td>
                            <td>
                            <div id="HANA">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'HANA';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>MySQL</td>
                            <td>
                            <div id="MySQL">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'MySQL';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>MariaDB</td>
                            <td>
                            <div id="MariaDB">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'MariaDB';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>PostgreSQL</td>
                            <td>
                            <div id="PostgreSQL">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'PostgreSQL';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Sybase IQ</td>
                            <td>
                            <div id="SybaseIQ">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'SybaseIQ';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Sybase ASE</td>
                            <td>
                            <div id="SybaseASE">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'SybaseASE';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Sybase Anywhere</td>
                            <td>
                            <div id="SybaseAny">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'SybaseAnywhere';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Informix</td>
                            <td>
                            <div id="Informix">
                                <select onchange="dbShow(this.parentNode.id,this)">
                                  <option value="NoData">- Select -</option>
                                    <?php
                                    $res = mysqli_query($link, "select distinct dbVer from permutations where dbType like 'Informix';");
                                    while ($row = mysqli_fetch_array($res)) {
                                        ?>
                                        <option><?php echo $row["dbVer"]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>


                    </table>
                </div>
            </div>
            <div>
                <lable id="dbLbl1">Your choose: <H1 id="dbLbl2"></H1></lable>
                <a id="backDbContainer" onclick="backToDbContainer()" style="color:white" ;>Back</a>
            </div>
        </div>

        <div class="column" style="background-color:#ccc;">
            <div>
                <div>
                    <h2>Choose resources</h2>
                    <h5>Pick your resources in VSphere</h5>
                    <!-- <p>You logged in as: <label id="nameOfUser"></label></p> -->
                    <table class="table table-hover">

                        <tr>
                            <td>MNG Network:</td>
                            <td>
                              <select onselect="dbShow(this.parentNode.id,this)">
                                <option value="">- Select -</option>
                                  <?php
                                  $res = mysqli_query($link, "select distinct mng from resources;");
                                  while ($row = mysqli_fetch_array($res)) {
                                      ?>
                                      <option><?php echo $row["mng"]; ?></option>
                                      <?php
                                  }
                                  ?>
                              </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Resource pool:</td>
                            <td>
                              <select onselect="dbShow(this.parentNode.id,this)">
                                <option value="">- Select -</option>
                                  <?php
                                  $res = mysqli_query($link, "select distinct rp from resources;");
                                  while ($row = mysqli_fetch_array($res)) {
                                      ?>
                                      <option><?php echo $row["rp"]; ?></option>
                                      <?php
                                  }
                                  ?>
                              </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Data Store:</td>
                            <td>
                              <select onselect="dbShow(this.parentNode.id,this)">
                                <option value="">- Select -</option>
                                  <?php
                                  $res = mysqli_query($link, "select distinct ds from resources;");
                                  while ($row = mysqli_fetch_array($res)) {
                                      ?>
                                      <option><?php echo $row["ds"]; ?></option>
                                      <?php
                                  }
                                  ?>
                              </select>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="osDbDetails">
    <button class="collapsible">Operation Systems</button>
    <div class="content">
      <section class="container-fluid">
        <!-- <div class="container-fluid"> -->
          <div class="pmain1" id="os">
            <p>To find which OS are supported, please go to: <a href="osdb_pages/osdbtable.php">OS Details</a></p>
            <div class="row">
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\rhel.png" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\centos.jpg" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\red-hat.png" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\solaris.jpg" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\ubuntu.jpg" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\2019.jpg"  class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\hpux.jpg"  class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\OS\aix.jpg"  class="img-circle" alt="OS"></a>
                </div>
            </div>
          </div>
      </section>
    </div>

    <button class="collapsible">Databases</button>
    <div class="content">
      <section class="container-fluid">
          <div class="pmain2" id="db">
            <p>To find which DB are supported, please go to: <a href="osdb_pages/osdbtable.php">DB Details</a></p>
            <div class="row">
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\mySQL.png" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\oracledb.png" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\mariadb.jpg" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\mongodb.png" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\sqlserver.jpg" class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\teradata.jpg"  class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\claudera.jpg"  class="img-circle" alt="OS"></a>
                </div>
                <div class="circle-img responsive">
                  <a href="#" class="col-md-2"><img src="resources\DB\informix.jpeg"  class="img-circle" alt="OS"></a>
                </div>
            </div>
          </div>
      </section>
  </div>
</section>

<!-- The Modal -->
<div id="modalView" class="modal">
    <div class="modal-content card card-5">
        <header>
            <div class="card-heading">
                <h2 class="title">Login</h2>
            </div>
        </header>
        <div class="card-body">
            <form method="POST" action="osdb_pages/login.php">
                <article class="form-row">
                    <div class="name">Email</div>
                    <div class="value">
                        <div class="input-group">
                            <input class="input--style-5" type="email" id="emailid" name="email">
                        </div>
                    </div>
                </article>
                <article class="form-row">
                    <label>If it's your first time, please <a href="osdb_pages/register.html">register</a></label>
                </article>
                <button class="btn btn--radius-2 btn-success" type="submit" style="margin-left: -5px">Login</button>
                <a class="btn btn--radius-2 btn-danger" href="./index.php" role="button">Cancel</a>
            </form>
        </div>
    </div>
</div>

<script>
        // Get the modal
        var modal = document.getElementById('modalView');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
</script>

<script>

        function osShow(father_id, selected_id) {
            var value = selected_id.value;
            document.getElementById('osContainer').style.visibility = 'hidden';
            document.getElementById('osLbl1').style.visibility = 'visible';
            document.getElementById('backOsContainer').style.visibility = 'visible';
            document.getElementById('osLbl2').innerHTML = father_id + value;
        }

        function dbShow(father_id, selected_id) {
            var value = selected_id.value;
            document.getElementById('dbContainer').style.visibility = 'hidden';
            document.getElementById('dbLbl1').style.visibility = 'visible';
            document.getElementById('backDbContainer').style.visibility = 'visible';
            document.getElementById('dbLbl2').innerHTML = father_id + value;
        }

        function backToOsContainer() {
            document.getElementById('backOsContainer').style.visibility = 'hidden';
            document.getElementById('osLbl1').style.visibility = 'hidden';
            document.getElementById('osContainer').style.visibility = 'visible';
        }

        function backToDbContainer() {
            document.getElementById('backDbContainer').style.visibility = 'hidden';
            document.getElementById('dbLbl1').style.visibility = 'hidden';
            document.getElementById('dbContainer').style.visibility = 'visible';
        }
  </script>

  <script>
        var coll = document.getElementsByClassName("collapsible");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.maxHeight) {
                    content.style.maxHeight = null;
                } else {
                    content.style.maxHeight = content.scrollHeight + "px";
                }
            })};
</script>


<footer data-include="footer.html"></footer>

</body>
</html>

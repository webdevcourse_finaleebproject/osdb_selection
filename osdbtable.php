<!DOCTYPE html>
<html lang="en">
<style>

#search {
	float:right;
	padding-right:5%;
}

mark {
  background: orange;
  color: black;
}

</style>

<head>
    <script src="../js/csi.js"></script>
    <meta charset="UTF-8">
    <title>Databases List</title>
    <!-- MDB icon -->
    <!-- <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Carter One' rel='stylesheet'>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-responsive.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel='stylesheet' type='text/css' href='../css/osdb.css'/>
    <script type="application/javascript">
        const data = {
            "databases": {
                "name": "database",
                "display_name": "Database",
                "name_column_d_name": "Name",
                "version_column_d_name": "Version",
                "products": {
                    "Amazon Aurora MySql": ["8.0", "5.7", "5.6"],
                    "Amazon Aurora PostgreSQL": ["10.7"],
                    "Amazon RDS for MariaDB Server": ["10.0.24 or newer"],
                    "Amazon RDS for MySql": ["5.7.x (minimal version 5.7.16)", "5.6"],
                    "Amazon RDS for Oracle": ["12.2", "12.1", "11.2"],
                    "Amazon RDS for PostgreSQL": ["11.x", "10.x", "9.6.x (minimal version 9.6.11)", "9.5.x (minimal version 9.5.7)"],
                    "Azure SQL DB": ["All"],
                    "Cloudera Hadoop (CDH)": ["6.2", "6.1", "6.0", "5.16", "5.15", "5.14", "5.13", "5.12"],
                    "Datastax Cassandra": ["6.7", "6.0", "5.1"],
                    "Hortonworks Hadoop (HDP)": ["3.1", "3.0", "2.6"],
                    "IBM Db2 for i": ["7.4", "7.3", "7.2"],
                    "IBM Db2 for LUW": ["11.5", "11.1", "10.5", "10.1", "9.7"],
                    "IBM Db2 for z/OS": ["12", "11", "10"],
                    "IBM Information Management System (IMS)": ["15", "14", "13", "12"],
                    "Informix": ["14.10", "12.10", "11.7"],
                    "MariaDB Server": ["10.4", "10.3", "10.2", "10.1"],
                    "Microsoft SQL Server": ["2017", "2016", "2014", "2012"],
                    "MongoDB": ["4.0", "3.6"],
                    "MongoDB Atlas": ["All"],
                    "MySQL": ["8.0", "5.7", "5.6"],
                    "Oracle": ["19c", "18c", "12.2", "12.1", "11.2"],
                    "Pivotal Greenplum Database": ["6.x", "5.x", "4.3.x"],
                    "PostgreSQL": ["12.x", "11.x", "10.x", "9.6", "9.5"],
                    "SAP-HANA": ["2 SPS 4", "2 SPS 3", "2 SPS 2", "1 SPS 12"],
                    "Sybase ASE": ["16.0", "15.7"],
                    "Sybase IQ": ["16.1 SP4", "16.1 SP2", "16.0 SP11"],
                    "Sybase SQL Anywhere": ["17.0"],
                    "Teradata": ["16.20", "16.10"]
                }
            },
            "oprationSystems": {
                "name": "oprationSystems",
                "display_name": "Operating System",
                "name_column_d_name": "Name",
                "version_column_d_name": "Version",
                "products": {
                    "CentOS": ["7 (x86_64)", "6 (x86_64)", "6 (x86)", "5 (x86_64)", "5 (x86)"],
                    "HP-UX": ["11.31 (Itanium)", "11.31 (PARISC)"],
                    "IBM AIX (POWER)": ["7.2", "7.1"],
                    "IBM i (POWER)": ["7.4", "7.3", "7.2"],
                    "IBM z/OS": ["1.13 or newer"],
                    "Microsoft Windows (x86_64)": ["2019", "2016", "2012 R2", "2012"],
                    "Oracle Linux (x86_64)": ["7", "6", "5"],
                    "Oracle Linux UEK (x86_64)": ["7 UEK 5 (4.14.35 kernel family)", "7 UEK 4 (4.1.12 kernel family)", "7 UEK 3 (3.8.13 kernel family)", "6 UEK 4 (4.1.12 kernel family)", "6 UEK 3 (3.8.13 kernel family)", "6 UEK 2 (2.6.39 kernel family)", "5 UEK 2 (2.6.39 kernel family)"],
                    "Red Hat EL": ["7 (x86_64)", "6 (x86_64)", "6 (x86)", "5 (x86_64)", "5 (x86)", "5 (x86_64 XEN)", "5 (x86 PAE)"],
                    "SLES [SUSE] (x86_64)": ["12 SP 5", "12 SP 4", "12 SP 3", "12 SP 2", "11 SP 4", "11 SP 3"],
                    "Solaris": ["11 (Sparc)", "11 (x86_64)", "10 (Sparc)", "10 (x86_64)"],
                    "Ubuntu (x86_64)": ["16.04"]
                }
            },
        }
        $(document).ready(function () {
            let dbText = ''
            for (let database in data['databases']['products']) {
                dbText += '<div class="row"><div class="tablerow col-sm-12 text-center">' + database + '</div></div>'
            }
            $('#databases').html(dbText)

            let osText = ''
            for (let os in data['oprationSystems']['products']) {
                osText += '<div class="row"><div class="tablerow col-sm-12 text-center">' + os + '</div></div>'
            }
            $('#operatingSystems').html(osText)
        });

    </script>
</head>
<body class="bg-orange">
<header data-include="headerF.html"></header>

<div class="container bg-light" id="context">
    <div class="row text-center">
        <div class="col-sm-6">
            <h2 style="margin:0.5em;">Databases</h2>
            <div id="databases">

            </div>
        </div>
        <div class="col-sm-6">
            <h2 style="margin:0.5em;">Operating Systems</h2>
            <div id="operatingSystems">

            </div>
        </div>
    </div>

</div>
<footer data-include="footerF.html"></footer>
</body>
</html>

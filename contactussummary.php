<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $_SESSION['subject'] = $_POST["subject"];
    $_SESSION['message'] = $_POST["message"];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Submit</title>
    <link href="../css/form.css" rel="stylesheet" media="all">
    <script>
        function appSubmited() {
            alert("Thank you for writing to us! Your application will be addressed as soon as possible.");
        }
    </script>
    <script src="../js/csi.js"></script>
</head>
<body>
<header data-include="headerF.html"></header>
<main class="page-wrapper bg-gra-03 p-t-45 p-b-50">
    <div class="wrapper wrapper--w790">
        <div class="card card-5">
            <header>
                <div class="card-heading">
                    <h2 class="title">Contact Us - Summary</h2>
                </div>
            </header>
            <div class="card-body">
                <form method="POST" action="contactussubmit.php">
                    <article class="form-row">
                        <div class="name">Full Name:</div>
                        <div class="value">
                            <span><?php echo $_COOKIE['first_name'] , " ", $_COOKIE['last_name'] ?></span>
                        </div>
                    </article>
                    <article class="form-row">
                        <div class="name">Email Address:</div>
                        <div class="value">
                            <span><?php echo $_COOKIE['email']?></span>
                        </div>
                    </article>
                    <article class="form-row">
                        <div class="name">Subject:</div>
                        <div class="value">
                            <?php
                            echo $_SESSION['subject'];
                            ?>
                        </div>
                    </article>
                    <article class="form-row">
                        <div class="name">Your Message:</div>
                        <div class="value">
                            <span><?php echo $_SESSION['message']?></span>
                        </div>
                    </article>
                    <div>
                        <button class="btn btn--radius-2 btn--green" type="submit" onclick="appSubmited()">Submit</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <a class="btn btn--radius-2 btn-danger" href="../index.php" role="button">Cancel</a></main>
</main>

<footer data-include="footerF.html"></footer>
</body>
</html>
